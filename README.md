<h1 align="center">OPS 专用小键盘</h1>

<p align="center"><img src="https://img.shields.io/badge/Licence-MIT-green.svg?style=for-the-badge" /></p>

<p align="center"><img src="https://gitee.com/walkline/keypad-for-ops/raw/master/images/appearance/preview_01.png" /></p>

### 项目说明

主控为合宙 [Air001芯片](https://wiki.luatos.com/chips/air001/mcu.html)，固件使用 [Arduino](https://www.arduino.cc/) 进行开发，按键扫描使用 [瀚文键盘](https://oshwhub.com/pengzhihui/b11afae464c54a3e8d0f77e1f92dc7b7) 的 `1*n`扫描方案，串口转 HID 交互使用沁恒 [CH9329芯片](https://www.wch.cn/products/CH9329.html)。

> 电路原理图及 PCB 等硬件相关资料可以访问 [嘉立创EDA硬件开源平台](https://oshwhub.com/walkline/keypad-for-ops) 获取查看

### 使用场景

用于解决在需要设置一体机`BIOS 参数`但是又找不到键盘的问题。

### 按键组合及功能

![](./images/appearance/layout.png)

小键盘可以提供的组合键和组合键功能如下表所示：

| 按键 | 功能 | 说明 |
| :-: | :-: | :-: |
| <kbd>Del</kbd> , <kbd>F2</kbd> , <kbd>F10</kbd> | 开机进入`BIOS` | 对于大多数机器来说够用了 |
| <kbd>Ctrl</kbd> + <kbd>Alt</kbd> + <kbd>Del</kbd> | 热启动 | |
| <kbd>方向键</kbd> | 移动光标 | |
| <kbd>Enter</kbd> | 进入下一级设置项 | 或打开设置详情 |
| <kbd>Esc</kbd> | 返回上一级设置项 | 或关闭设置详情 |
| <kbd>F10</kbd> | 退出并保存设置 | 需要回车键确认 |
| <kbd>Ctrl</kbd> + <kbd>F3</kbd> | 某些`OPS`的一键**备份**功能 | |
| <kbd>Ctrl</kbd> + <kbd>F4</kbd> | 某些`OPS`的一键**还原**功能 | |

### 如何烧录固件

* 一种方法是安装`Arduino IDE`后，再 [安装 AirMCU 支持包](https://arduino.luatos.com/getting_started/install.html#%E5%AE%89%E8%A3%85-airmcu-%E6%94%AF%E6%8C%81%E5%8C%85)，打开项目中的`.ino`文件，设置`开发板`为`Air001 Dev Chip`、`时钟源和频率`为`双 16M`，然后点击`上传`即可。

	> ![](./images/firmware/arduino_ide_settings.png)

* 另一种方法是使用烧录工具，下载 [AirISP-next](https://github.com/Air-duino/AirISP-next/releases) 烧录工具，将压缩包内的文件夹解压缩到`flash`文件夹，双击`flash.cmd`文件进行烧录。

	`flash`文件夹目录结构如下：

	```bash
	├── AirISP-next
	│   └── AirISP-next.exe
	├── firmware.bin
	├── flash.cmd
	└── flash.py
	```

	> 详细教程请参考 [刷机烧录教程](https://wiki.luatos.com/chips/air001/mcu.html#id3)

### 灯光效果

灯光效果目前实现了两种模式：

- `渐变过渡`
- `彩虹效果`

按住任意键后给设备上电，可以切换不同的模式。

### 实物照片

| ![](./images/appearance/photo_01.png) | ![](./images/appearance/photo_02.png) |
| :-: | :-: |
| ![](./images/appearance/photo_03.png) | ![](./images/appearance/photo_04.png) |

### 已知问题

* `合宙 Air001`动态内存只有`4K`，在保证稳定高效运行的前提下最多只能点亮 13 颗 LED

* ~~读取`74HC165`芯片数据时特定引脚（D7）会导致程序重启，怀疑是因为使用了不同厂家的芯片导致的，已修复~~

### 合作交流

* 联系邮箱：<walkline@163.com>
* QQ 交流群：
	* 走线物联：[163271910](https://jq.qq.com/?_wv=1027&k=xtPoHgwL)
	* 扇贝物联：[31324057](https://jq.qq.com/?_wv=1027&k=yp4FrpWh)

<p align="center"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_walkline.png" width="300px" alt="走线物联"><img src="https://gitee.com/walkline/WeatherStation/raw/docs/images/qrcode_bigiot.png" width="300px" alt="扇贝物联"></p>
