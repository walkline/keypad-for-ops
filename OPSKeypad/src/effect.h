/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __EFFECT__
#define __EFFECT__

#include "Arduino.h"
#include "src/common.h"

constexpr uint8_t LIGHT_EFFECT_COUNT = 2;

const uint8_t COLORS[7][3] = {{255, 0, 0},  {255, 165, 0}, {255, 255, 0},
                              {0, 255, 0},  {0, 127, 255}, {0, 0, 255},
                              {139, 0, 255}};

class EFFECT {
  public:
    uint8_t steps = 40;

  public:
    void gradient(uint8_t *color);

    void rainbow(uint8_t &index, uint8_t *color);

  private:
    void color_hsv(uint16_t hue, uint8_t *color);
};

#endif
