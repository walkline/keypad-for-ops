/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#include "src/hal/hal_def.h"

void DMA1_Channel1_IRQHandler() { HAL_DMA_IRQHandler(spi2_handle.hdmatx); }

void SPI2_IRQHandler() { HAL_SPI_IRQHandler(&spi2_handle); }
