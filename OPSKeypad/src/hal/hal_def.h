/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __HAL_DEF__
#define __HAL_DEF__

#ifdef __cplusplus
extern "C" {
#endif

#include "air001xx_hal.h"

extern DMA_HandleTypeDef hdma_tx;
extern SPI_HandleTypeDef spi2_handle;

#ifdef __cplusplus
}
#endif

#endif
