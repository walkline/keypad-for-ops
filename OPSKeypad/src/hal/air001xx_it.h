/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __AIR001XX_IT__
#define __AIR001XX_IT__

#ifdef __cplusplus
extern "C" {
#endif

void DMA1_Channel1_IRQHandler(void);
void SPI2_IRQHandler(void);

#ifdef __cplusplus
}
#endif

#endif
