/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __STORAGE__
#define __STORAGE__

#include "Arduino.h"
#include "src/common.h"
#include <EEPROM.h>

constexpr uint8_t SETTINGS_OK = 1;

class STORAGE {
  public:
    struct settings_t {
        uint8_t status;
        uint8_t layer;
        uint8_t light_effect;
        uint8_t light_speed;
    } settings;

    STORAGE() { settings = {0, 0, 0, 0}; }

  private:
    uint8_t address = 0;

  public:
    bool load_settings(void);

    void save_settings(void);
};

#endif
