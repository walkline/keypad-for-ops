/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __74HC165__
#define __74HC165__

#include "src/common.h"
#include <SPI.h>

class _74HC165 {
  public:
    enum key_status_e : uint8_t { PRESSED, RELEASED };

    uint8_t key_buffer[IO_COUNT / 8] = {};

  public:
    /* 初始化 SPI 外设和控制引脚 */
    void setup(void);

    /* 获取滤波后的按键状态 */
    void get_keys_status(void);

  private:
    /* 扫描按键当前状态 */
    void scan_keys(uint8_t *buffer);
};

#endif
