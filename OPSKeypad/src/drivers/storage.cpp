/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#include "storage.h"

bool STORAGE::load_settings() {
    EEPROM.get(address, settings);
    bool result = settings.status == SETTINGS_OK;

#if DEBUG_PROMPT
    result ? Serial.println("Settings loaded")
           : Serial.println("Load default settings");
#endif

    return result;
}

void STORAGE::save_settings() {
    EEPROM.put(address, settings);

#if DEBUG_PROMPT
    Serial.println("Settings saved");
#endif
}
