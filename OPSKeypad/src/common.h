/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __COMMON__
#define __COMMON__

#include "Arduino.h"

/* 调试数据显示开关 */
#define DEBUG_DATA 0

/* 提示信息显示开关 */
#define DEBUG_PROMPT 1

/* 74HC165 */
#define KEYS_MOSI PA7 /* not used */
#define KEYS_MISO PA6
#define KEYS_CLK PA5
#define KEYS_PL PB2
#define KEYS_CE PB0

/* WS2812 */
#define LEDS_MOSI PA4
#define LEDS_MISO PA9 /* not used */
#define LEDS_SCLK PB8 /* not used */

/* UART1 */
constexpr uint32_t UART1_BAUDRATE = 115200;
#define UART1_RXD PA3
#define UART1_TXD PA2

/* CH9329 */
constexpr uint32_t UART2_BAUDRATE = 9600;
#define UART2_RXD PA1
#define UART2_TXD PA0

constexpr uint8_t IO_COUNT = 2 * 8; /* 74HC165 * 2 */
constexpr uint8_t KEYS_COUNT = 13;
constexpr uint8_t LEDS_COUNT = 13;

constexpr uint8_t KEYS_FILTER_TIME = 100; /* 按键滤波消抖间隔时间(us) */

#endif
