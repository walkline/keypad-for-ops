/**********************************************************
 * Copyright © 2024 Walkline Wang (https://walkline.wang)
 * Gitee: https://gitee.com/walkline/keypad-for-ops
 **********************************************************/
#ifndef __KEYCODE__
#define __KEYCODE__

// https://github.com/peng-zhihui/HelloWord-Keyboard/blob/main/2.Firmware/HelloWord-Keyboard-fw/HelloWord/hw_keyboard.h#L32
enum keycode_e : uint8_t {
    RESERVED = 0, ERROR_ROLL_OVER, POST_FAIL, ERROR_UNDEFINED,
    A, B, C, D, E, F, G, H, I, J, K, L, M,
    N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
    NUM_1/*1!*/, NUM_2/*2@*/, NUM_3/*3#*/, NUM_4/*4$*/, NUM_5/*5%*/,
    NUM_6/*6^*/, NUM_7/*7&*/, NUM_8/*8**/, NUM_9/*9(*/, NUM_0/*0)*/,
    ENTER, ESC, BACKSPACE, TAB, SPACE,
    MINUS/*-_*/, EQUAL/*=+*/, LEFT_U_BRACE/*[{*/, RIGHT_U_BRACE/*]}*/,
    BACKSLASH/*\|*/, NONE_US/**/, SEMI_COLON/*;:*/, QUOTE/*'"*/,
    GRAVE_ACCENT/*`~*/, COMMA/*, <*/, PERIOD/*.>*/, SLASH/*/?*/,
    CAP_LOCK, F1, F2, F3, F4, F5, F6, F7, F8, F9, F10, F11, F12,
    PRINT, SCROLL_LOCK, PAUSE, INSERT, HOME, PAGE_UP, DELETE, END, PAGE_DOWN,
    RIGHT_ARROW, LEFT_ARROW, DOWN_ARROW, UP_ARROW, PAD_NUM_LOCK,
    PAD_SLASH, PAD_ASTERISK, PAD_MINUS, PAD_PLUS, PAD_ENTER,
    PAD_NUM_1, PAD_NUM_2, PAD_NUM_3, PAD_NUM_4, PAD_NUM_5,
    PAD_NUM_6, PAD_NUM_7, PAD_NUM_8, PAD_NUM_9, PAD_NUM_0,
    PAD_PERIOD, NONUS_BACKSLASH, APPLICATION,

    KB_CONTROL_KEYS_SEPARATOR = 120,

    LEFT_CTRL, LEFT_SHIFT, LEFT_ALT, LEFT_GUI,
    RIGHT_CTRL, RIGHT_SHIFT, RIGHT_ALT, RIGHT_GUI,

    KB_SEPARATOR = 140,

    LEFT_CLICK, RIGHT_CLICK, MIDDLE_CLICK, /* MOUSE_CLICK */
    MOUSE_LEFT, MOUSE_RIGHT, MOUSE_UP, MOUSE_DOWN, /* MOUSE_MOVE */
    WHEEL_UP, WHEEL_DOWN, /* MOUSE_WHEEL */

    MS_SEPARATOR = 160,

    /* some media keys and others */

    MODE = 200, LAYER_UP, LAYER_DOWN, LIGHT_SPEED_UP, LIGHT_SPEED_DOWN
};

#endif
